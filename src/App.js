import Canvas from "./components/canvas/Canvas";
import { useState } from "react";

function App() {
  const [canvasVisible, setCanvasVisible] = useState(true);

  return (
    <div>
      <div>
        <button onClick={() => setCanvasVisible(!canvasVisible)}>
          toggle canvas
        </button>
      </div>
      <div>{canvasVisible && <Canvas />}</div>
    </div>
  );
}

export default App;
